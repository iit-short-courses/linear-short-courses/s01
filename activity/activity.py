numbers = [1, 2, 3, 4, 5]
print(f"list of numbers: {numbers}\n")

meals = ['spam', 'Spam', 'SPAM!']
print(f"list of meals: {meals}\n")

messages = ["Hi!"] * 4
print(f"Result of repeat operator: {messages}\n")
# ----------------------------------------------------------------
print(f"List literals and operations using 'numbers' list")

print(f"length of the list: {len(numbers)}")

print(f"The list item removed from list: {numbers.pop()}")

numbers.reverse()
print(f"The list in reversed order is: {numbers}\n")
# -----------------------------------------------------------------
print(f"List literals and operations using 'meals' list:")

print(f"Using indexing operation: {meals[2]}")

print(f"Using negative indexing operation: {meals[-2]}")

meals[1] = "eggs"
print(f"Modified list: {meals}")

meals.append('bacon')
print(f"Added a new item in the list: {meals}")

meals.sort()
print(f"Modified list after sort method: {meals}\n")

# ------------------------------------------------------------------
recipe = {'eggs' : 3}
print("Created recipe dictionary:")
print(f"recipe: {recipe}\n")

recipe["spam"] = 2
recipe["ham"] = 1
recipe["brunch"] = 'bacon'
print(f"Modified recipe dictionary:\nrecipe = {recipe}\n")

recipe["ham"] = ['grill', 'bake', 'fry']
print(f"Updated the 'ham' key value:\nrecipe = {recipe}\n")

del recipe["eggs"]
print(f"Modified recipe after deleting 'eggs' key:\nrecipe = {recipe}\n")

# -----------------------------------------------------------------------

bob = 	{'name': {'first': 'Bob', 'last': 'Smith'},
		 'age': 42,
		 'job': ['software', 'writing'],
		 'pay': (40000, 50000)}

print("Given 'bob' dictionary:")
print(f"{bob}\n")

print(f"Accessing the value of 'name' key: {bob['name']}")
print(f"Accessing the value of 'last' key: {bob['name']['last']}") # Cant use \" inside an index..syntax invalid
print(f"Accessing the second value of 'pay' key: {bob['pay'][1]}\n")

# -------------------------------------------------------------------------

print(f"Using 'numeric' tuple:")
numeric = ('twelve', 3.0, [11, 22, 33])
print(f"numeric = {numeric}\n")

print(f"Accessing tuple item: {numeric[1]}")
print(f"Accessing tuple item: {numeric[2][1]}")
